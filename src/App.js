// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import UsersPage from './UserPage';

function App() {
  return (
    <Router>
      <div>
        <Routes>
          <Route path="/users" element={<UsersPage />} />
        </Routes>
      </div>
      <div>
        <Link to="/users">Users</Link>
      </div>
    </Router>
  );
}

export default App;
